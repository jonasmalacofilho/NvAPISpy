// NvAPI.cpp : Defines the exported functions for the DLL application.
// This DLL project is a mock implementation of the NVidia NvAPI for spying on NvAPI calls
// It should also be able to forward calls to the real NvAPI

#include <Windows.h>
#include "nvapi.h"
#include <fstream>
#include <string>

// Location of real NvAPI DLL files
#ifdef _WIN64
#define NVAPIDLL_1      "C:\\Windows\\System32\\nvapi64_orig.dll"
#define NVAPIDLL_2      "C:\\Windows\\System32\\nvapi64.dll"
#else
#define NVAPIDLL_1      "C:\\Windows\\System32\\nvapi_orig.dll"
#define NVAPIDLL_2      "C:\\Windows\\System32\\nvapi.dll"
#endif

#define NVAPI_ERROR 1

// Pointers to real NvAPI SDK DLL functions
typedef NV_STATUS  (*NVAPI_INITIALIZE)(void);
typedef void*      (*NVAPI_QUERYINTERFACE)(unsigned int function_code);
typedef NV_STATUS  (*NVAPI_I2CWRITEEX)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown);
typedef NV_STATUS  (*NVAPI_I2CREADEX)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown);

// Module pointer for real NvAPI
HMODULE hModule = NULL;
bool opened_file = false;

std::ofstream outfile;

NVAPI_INITIALIZE        _NvAPI_Initialize;
NVAPI_QUERYINTERFACE    _NvAPI_QueryInterface;
NVAPI_I2CWRITEEX        _NvAPI_I2CWriteEx;
NVAPI_I2CREADEX         _NvAPI_I2CReadEx;

extern "C"
{
    __declspec(dllexport) void* nvapi_QueryInterface(unsigned int function_code)
    {
        void* ret_val = NULL;

        printf("nvapi_QueryInterface\r\n");
        if (!hModule)
        {
            hModule = LoadLibrary(NVAPIDLL_1);

            if (!hModule)
            {
                hModule = LoadLibrary(NVAPIDLL_2);
            }

            if (hModule)
            {
                _NvAPI_QueryInterface = (NVAPI_QUERYINTERFACE)GetProcAddress(hModule, "nvapi_QueryInterface");

                _NvAPI_I2CWriteEx     = (NVAPI_I2CWRITEEX)_NvAPI_QueryInterface(0x283AC65A);
                _NvAPI_I2CReadEx      = (NVAPI_I2CREADEX)_NvAPI_QueryInterface(0x4D7B0709);
            }
        }

        if (hModule)
        {
            // Get actual value
            ret_val = _NvAPI_QueryInterface(function_code);

            // Override if it's a function we fake
            switch (function_code)
            {
            case 0x283AC65A:
                ret_val = &NvAPI_I2CWriteEx;
                break;

            case 0x4D7B0709:
                ret_val = &NvAPI_I2CReadEx;
                break;
            }
        }

        return(ret_val);
    }
}

NV_STATUS NvAPI_I2CWriteEx(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if(!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if(hModule)
    {
        char hex_buf[7];

        snprintf(hex_buf, 5, "0x%02X", i2c_info->i2c_dev_address >> 1);
        outfile << "NvAPI_I2CWriteEx: Dev: ";
        outfile << hex_buf;

        snprintf(hex_buf, 5, "0x%02X", i2c_info->reg_addr_size);
        outfile << " RegSize: ";
        outfile << hex_buf;

        outfile << " Reg:";

        for (unsigned int i = 0; i < i2c_info->reg_addr_size; i++)
        {
            outfile << " ";
            snprintf(hex_buf, 5, "0x%02X", i2c_info->i2c_reg_address[i]);
            outfile << hex_buf;
        }

        snprintf(hex_buf, 5, "0x%02X", i2c_info->size);
        outfile << " Size: ";
        outfile << hex_buf;

        outfile << " Data:";

        for (unsigned int i = 0; i < i2c_info->size; i++)
        {
            outfile << " ";
            snprintf(hex_buf, 5, "0x%02X", i2c_info->data[i]);
            outfile << hex_buf;
        }

        outfile << std::endl;

        ret_val = _NvAPI_I2CWriteEx(physical_gpu_handle, i2c_info, unknown);
    }

    return(ret_val);
}

NV_STATUS NvAPI_I2CReadEx(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        char hex_buf[7];

        ret_val = _NvAPI_I2CReadEx(physical_gpu_handle, i2c_info, unknown);

        snprintf(hex_buf, 5, "0x%02X", i2c_info->i2c_dev_address >> 1);
        outfile << "NvAPI_I2CReadEx:  Dev: ";
        outfile << hex_buf;

        snprintf(hex_buf, 5, "0x%02X", i2c_info->reg_addr_size);
        outfile << " RegSize: ";
        outfile << hex_buf;

        outfile << " Reg:";

        for (unsigned int i = 0; i < i2c_info->reg_addr_size; i++)
        {
            outfile << " ";
            snprintf(hex_buf, 5, "0x%02X", i2c_info->i2c_reg_address[i]);    
            outfile << hex_buf;
        }
        
        snprintf(hex_buf, 5, "0x%02X", i2c_info->size);
        outfile << " Size: ";
        outfile << hex_buf;

        outfile << " Data:";

        for (unsigned int i = 0; i < i2c_info->size; i++)
        {
            outfile << " ";
            snprintf(hex_buf, 5, "0x%02X", i2c_info->data[i]);
            outfile << hex_buf;
        }

        outfile << std::endl;
    }

    return(ret_val);
}